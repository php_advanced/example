<?php

namespace oop\AbstractFactory;

interface Furniture
{
    public function hasLegs();
}