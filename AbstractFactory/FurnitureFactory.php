<?php

namespace oop\AbstractFactory;

interface FurnitureFactory
{
    public function produceChairs();
    public function produceSofas();
}