<?php


namespace oop\FabricMethod;

class DevelopmentFactory extends HiringManager
{
    public function makeInterviewer(): Interviewer
    {
        return new Developer();
    }
}

$developerFactory = new DevelopmentFactory();

$developer = $developerFactory->makeInterviewer();

$developer->askQuestion();