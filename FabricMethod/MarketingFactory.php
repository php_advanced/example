<?php


namespace oop\FabricMethod;

class MarketingFactory extends HiringManager
{
    public function makeInterviewer(): Interviewer
    {
        return new Marketologist();
    }
}


