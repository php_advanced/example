<?php

namespace oop\SimpleFactory;

interface Door
{
    public function getWidth(): float;
    public function getHeight(): float;
}